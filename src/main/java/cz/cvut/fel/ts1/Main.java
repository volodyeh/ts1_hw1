package cz.cvut.fel.ts1;

public class Main {
    public static void main(String[] args) {
        Volodyeh volodyeh = new Volodyeh();

        System.out.println(volodyeh.factorial(4));
        System.out.println(volodyeh.factorial(5));
        System.out.println(volodyeh.factorial(6));
    }
}
