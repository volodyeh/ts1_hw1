package cz.cvut.fel.ts1;

public class Volodyeh {
    public long factorial(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("Number must be non negative");
        }

        long result = 1;
        for (int i = n; i > 0; i--) {
            result *= i;
        }
        return result;
    }



}
