package cz.cvut.fel.ts1;

import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;

public class VolodyehTest {

    @Test
    public void testFactorial5() {
        Volodyeh volodyeh = new Volodyeh();
        assertEquals(120, volodyeh.factorial(5));
    }

    @Test
    public void testFactorial0() {
        Volodyeh volodyeh = new Volodyeh();
        assertEquals(120, volodyeh.factorial(5));
    }

    @Test
    public void testFactorialNegativeNumber() {
        Volodyeh volodyeh = new Volodyeh();
        assertThrows(IllegalArgumentException.class, () -> volodyeh.factorial(-10));
    }

}